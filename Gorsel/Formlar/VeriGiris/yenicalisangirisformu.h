#ifndef YENICALISANGIRISFORMU_H
#define YENICALISANGIRISFORMU_H

#include <QDialog>

#include<VeriSiniflari/calisan.h>

namespace Ui {
class YeniCalisanGirisFormu;
}

class YeniCalisanGirisFormu : public QDialog
{
    Q_OBJECT

public:
    explicit YeniCalisanGirisFormu(QWidget *parent = nullptr);
    ~YeniCalisanGirisFormu();

private:
    Ui::YeniCalisanGirisFormu *ui;


    Calisan::Ptr _calisan;
};

#endif // YENICALISANGIRISFORMU_H
