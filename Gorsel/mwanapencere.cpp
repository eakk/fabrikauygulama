#include "mwanapencere.h"
#include "ui_mwanapencere.h"

#include<QMessageBox>

#include<Formlar/VeriGiris/yenicalisangirisformu.h>

MwAnaPencere::MwAnaPencere(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MwAnaPencere)
{
    ui->setupUi(this);
}

MwAnaPencere::~MwAnaPencere()
{
    delete ui;
}

void MwAnaPencere::close()
{
   auto cevap= QMessageBox :: question(this, "ÇIKIŞ ONAYI", "Çıkmak istediğine eminmisin?",
                            QMessageBox::Yes | QMessageBox::No,QMessageBox::No);
   if(cevap==QMessageBox::Yes){

   QMainWindow::close();

   }
}



void MwAnaPencere::on_actionCalisan_ekle_triggered()
{
    YeniCalisanGirisFormu form;
    form.exec();
}
