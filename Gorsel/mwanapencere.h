#ifndef MWANAPENCERE_H
#define MWANAPENCERE_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MwAnaPencere; }
QT_END_NAMESPACE

class MwAnaPencere : public QMainWindow
{
    Q_OBJECT

public:
    MwAnaPencere(QWidget *parent = nullptr);
    ~MwAnaPencere();
public slots:
    virtual void close();

private slots:
    void on_actionA_triggered();

    void on_actionCalisan_ekle_triggered();

private:
    Ui::MwAnaPencere *ui;
};
#endif // MWANAPENCERE_H
